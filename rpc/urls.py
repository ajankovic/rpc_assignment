from django.conf.urls import url

from .views import AdditionView

urlpatterns = [
    url(r'^$', AdditionView.as_view(), name='rpc-index'),
]
