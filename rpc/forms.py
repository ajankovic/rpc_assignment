from django import forms


class AdditionForm(forms.Form):
    FUNCTIONS = (
        ('add', '+'),
        ('sub', '-'),
        ('mul', '*'),
        ('div', '/'),
    )
    a = forms.DecimalField()
    function = forms.ChoiceField(choices=FUNCTIONS)
    b = forms.DecimalField()
