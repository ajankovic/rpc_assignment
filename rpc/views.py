from django.shortcuts import render
from django.views.generic import View
from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin

from .forms import AdditionForm
from rpcserver.client import RabbitMQClient, RpcTimeoutException


class AdditionView(LoginRequiredMixin, View):
    login_url = settings.LOGIN_URL
    client = RabbitMQClient(
            getattr(settings, 'RPC_RABBITMQ_ADDR', 'localhost'))

    def get(self, request):
        form = AdditionForm()
        return render(request, 'rpc/index.html', {'form': form})

    def post(self, request):
        form = AdditionForm(request.POST)
        if form.is_valid():
            a = form.cleaned_data['a']
            b = form.cleaned_data['b']
            fun = form.cleaned_data['function']
            try:
                if fun == 'add':
                    response = self.client.add(a, b)
                elif fun == 'sub':
                    response = self.client.sub(a, b)
                elif fun == 'mul':
                    response = self.client.mul(a, b)
                elif fun == 'div':
                    response = self.client.div(a, b)
                else:
                    raise Exception("Bad request")
            except RpcTimeoutException as ex:
                return render(request, 'rpc/error.html',
                              {
                                  'code': 'TIMEOUT',
                                  'message': str(ex),
                              }, status=500)
            except Exception as e:
                return render(request, 'rpc/error.html',
                              {
                                  'code': 'EXCEPTION',
                                  'message': repr(e),
                              }, status=500)
            if 'error' in response:
                return render(request, 'rpc/error.html',
                              {
                                  'code': response['error']['code'],
                                  'message': response['error']['message'],
                              }, status=500)
            return render(request, 'rpc/success.html',
                          {
                              'a': a,
                              'b': b,
                              'function': dict(form.fields['function'].choices)[fun],
                              'elapsed_time': response['elapsed_time'],
                              'result': response['result'],
                          }, status=200)
        return render(request, 'rpc/index.html', {'form': form})
