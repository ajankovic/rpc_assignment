from django.test import TestCase
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User

class RpcTest(TestCase):
    def setUp(self):
        user = User.objects.create_user('temporary', 'temporary@gmail.com', 'temporary')
        self.client.login(username='temporary', password='temporary')

    def test_index(self):
        response = self.client.get(reverse('rpc-index'))
        self.assertEqual(response.status_code, 200)

    def test_add(self):
        response = self.client.post(reverse('rpc-index'), {'a': 1, 'b': 2, 'function': 'add'})
        self.assertContains(response, "= 3", 1, 200)

    def test_sub(self):
        response = self.client.post(reverse('rpc-index'), {'a': 10, 'b': 9, 'function': 'sub'})
        self.assertContains(response, "= 1", 1, 200)

    def test_mul(self):
        response = self.client.post(reverse('rpc-index'), {'a': 1, 'b': 2, 'function': 'mul'})
        self.assertContains(response, "= 2", 1, 200)

    def test_div(self):
        response = self.client.post(reverse('rpc-index'), {'a': 2, 'b': 2, 'function': 'div'})
        self.assertContains(response, "= 1", 1, 200)