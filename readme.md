Django RPC implementation via RabbitMQ
======================================

Please use requirements.txt to setup dependencies

    pip install -r /path/to/requirements.txt

This project is developed and tested with Python 3.4.3 and Rabbitmq RabbitMQ 3.5.4

Quick start instructions
------------------------

Run rpc server first from the project directory with:

    python -m rpcserver.server

Then you can create database with:

    ./manage.py migrate

And create super user with:
    
    ./manage.py createsuperuser

Database is required because form is protected with super user privilegies.

Then you can run tests with:

    ./manage.py test

Or you can run server with:

    ./manage.py runserver

And browse http://localhost:8000