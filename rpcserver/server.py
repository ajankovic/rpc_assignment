import logging

from pika.adapters.twisted_connection import TwistedProtocolConnection
from pika.connection import ConnectionParameters
from twisted.internet import protocol, reactor, task
from twisted.python import log
import simplejson as json
import pika

from .calculation import calculate

class RabbitMQConsumer(object):
    QUEUE_NAME = 'rpc_queue'

    def on_connected(self, connection):
        d = connection.channel()
        d.addCallback(self.got_channel)
        d.addCallback(self.handle_deliveries)
        d.addErrback(log.err)

    def got_channel(self, channel):
        self.channel = channel
        self.channel.queue_declare(queue=self.QUEUE_NAME)
        self.channel.basic_qos(prefetch_count=1)
        return self.channel.basic_consume(queue=self.QUEUE_NAME)

    def handle_deliveries(self, queue_and_consumer_tag):
        queue, consumer_tag = queue_and_consumer_tag
        self.looping_call = task.LoopingCall(self.consume_from_queue, queue)

        return self.looping_call.start(0)

    def consume_from_queue(self, queue):
        d = queue.get()

        return d.addCallback(lambda result: self.handle_payload(*result))

    def handle_payload(self, channel, method, properties, body):
        logging.info('request received %s', body)
        req = json.loads(body)
        resp = {
            'jsonrpc': '2.0',
            'id': req['id']
        }
        try:
            res = calculate(req['method'], *req['params'])
            resp['result'] = res
        except Exception as e:
            logging.warn('exception %s', repr(e))
            resp['error'] = {
                'code': 'BAD_REQUEST',
                'message': repr(e)
            }

        resps = json.dumps(resp)
        logging.info('sending response %s', resps)
        channel.basic_publish(exchange='',
                              routing_key=properties.reply_to,
                              properties=pika.BasicProperties(
                                  correlation_id=properties.correlation_id),
                              body=resps)
        channel.basic_ack(delivery_tag=method.delivery_tag)


def runserver():
    logging.basicConfig(level=logging.INFO)
    consumer = RabbitMQConsumer()

    parameters = ConnectionParameters()
    cc = protocol.ClientCreator(reactor,
                                TwistedProtocolConnection,
                                parameters)
    logging.info('rpc server started')
    d = cc.connectTCP("localhost", 5672)
    d.addCallback(lambda protocol: protocol.ready)
    d.addCallback(consumer.on_connected)
    d.addErrback(log.err)
    reactor.run()

if __name__ == '__main__':
    runserver()
