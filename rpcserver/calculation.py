
def calculate(method, a, b):
    meth = getattr(Rpc, method, None)
    if not callable(meth):
        raise NotSupportedException("Remote procedure call '{}' is not supported.".format(meth))
    res = meth(a, b)
    return res


class Rpc(object):
    @classmethod
    def add(self, a, b):
        return a + b

    @classmethod
    def sub(self, a, b):
        return a - b

    @classmethod
    def mul(self, a, b):
        return a * b

    @classmethod
    def div(self, a, b):
        return a / b
