#!/usr/bin/env python
import uuid
from functools import partial

import simplejson as json
import pika
import time
import inspect

from .calculation import Rpc


class RabbitMQClient(object):
    """
    Usage:

    cl = RabbitMQClient('localhost')
    result = cl.add(2, 2)
    if 'error' in result:
        print 'Error'
    else:
        print 'Success {0}'.format(result['result'])
    """
    def __init__(self, host, timeout=2):
        # dynamically assign all rpc supported methods to the client
        for m in inspect.getmembers(Rpc, predicate=inspect.ismethod):
            if not m[0].startswith('_'):
                setattr(self, m[0], partial(self._call, name=m[0]))

        self.reqid = 1
        self.timeout = timeout
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(
            host=host))

        self.channel = self.connection.channel()

        result = self.channel.queue_declare(exclusive=True)
        self.callback_queue = result.method.queue

        self.channel.basic_consume(self._on_response, no_ack=True,
                                   queue=self.callback_queue)

    def _on_response(self, ch, method, props, body):
        if self.corr_id == props.correlation_id:
            self.response = body

    def _create_request_json(self, a, b, method):
        payload = {
            "jsonrpc": "2.0",
            "method": method,
            "params": [a, b],
            "id": self.reqid,
        }
        self.reqid += 1
        return json.dumps(payload, use_decimal=True)

    def _parse_response_json(self, response):
        return json.loads(response)

    def _call(self, a, b, name=''):
        self.response = None
        self.corr_id = str(uuid.uuid4())
        request_json = self._create_request_json(a, b, name)
        self.channel.basic_publish(exchange='',
                                   routing_key='rpc_queue',
                                   properties=pika.BasicProperties(
                                       reply_to=self.callback_queue,
                                       correlation_id=self.corr_id,
                                   ),
                                   body=request_json)
        start = time.time()
        while self.response is None:
            if time.time() - start > self.timeout:
                raise RpcTimeoutException("It took more than {0} seconds to get response".format(self.timeout))
            self.connection.process_data_events()
        resp = self._parse_response_json(self.response)
        resp['elapsed_time'] = round(time.time() - start, 5)
        return resp


class RpcTimeoutException(Exception):
    pass
